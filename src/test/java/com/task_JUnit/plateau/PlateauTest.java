package com.task_JUnit.plateau;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class PlateauTest {

  @Test
  void initSequence(){
    Plateau plateau = new Plateau(new Integer[]{444,222,444,333,444,999});
    assertThrows(ArrayIndexOutOfBoundsException.class,()->plateau.initSequence(-1));
  }


}
