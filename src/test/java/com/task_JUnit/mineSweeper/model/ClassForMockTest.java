package com.task_JUnit.mineSweeper.model;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ClassForMockTest {

  @InjectMocks
  ClassForMock classForMock;

  @Mock
  SomeService someService;

  @Test
  public void testSum(){
    when(someService.sum(5, 60)).thenReturn(65);
    assertEquals(classForMock.doubleSum(5,60), 130);
    verify(someService).sum(5,60);
  }
}
