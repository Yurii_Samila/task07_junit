package com.task_JUnit.mineSweeper.model;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.junit.Assume;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assumptions;
import org.junit.Assume;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class GameTest {
  @InjectMocks
  SomeService someService;

  @BeforeAll
  static void beforeAll(){
    System.out.println("Before all");
  }

  @BeforeEach
  void beforeEach(){
    System.out.println("Before each");
  }

  @Test
  void setColumn(){
  Game game = new Game();
  game.setColumns(2);
  assertEquals(4, game.getColumns(), "Columns has not set");
  }

  @Test
  public void setRows(){
    assumeTrue(false);
    Game game = new Game();
    game.setRows(5);
    assertEquals(6,game.getRows(),"Wrong number of rows");

  }

  @AfterEach
  void afterEach(){
    System.out.println("After each");
  }

  @AfterAll
  static void afterAll(){
    System.out.println("After all");
  }
}
