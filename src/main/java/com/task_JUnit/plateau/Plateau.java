package com.task_JUnit.plateau;

import java.util.Arrays;

public class Plateau {

  private Integer[] arrayOfIntegers;
  private int sequence;
  private int sequenceLength;
  private int sequenceIndex;

  public Plateau(Integer[] arrayOfIntegers) {
    this.arrayOfIntegers = arrayOfIntegers;
    sequenceIndex = -1;
  }

  public void findSequence(){
    if (arrayOfIntegers[0]>arrayOfIntegers[1]){
      initSequence(0);
    }
    if (arrayOfIntegers[arrayOfIntegers.length-1]>arrayOfIntegers[arrayOfIntegers.length-2]){
      initSequence(arrayOfIntegers.length-1);
    }
    for (int i = 1; i < arrayOfIntegers.length-1; i++) {
      if ((arrayOfIntegers[i]>arrayOfIntegers[i-1])&&(arrayOfIntegers[i]>arrayOfIntegers[i+1])){
        initSequence(i);
      }
    }
  }

  public void initSequence(int indexOfNumber){
    int number = arrayOfIntegers[indexOfNumber];
    int currentSequenceLength = findSequenceLength(number + "");
    if (currentSequenceLength>0){
      if (sequenceLength<currentSequenceLength){
        sequence = number;
        sequenceLength = currentSequenceLength;
        sequenceIndex = indexOfNumber;
      }
    }
  }

  private int findSequenceLength(String numberInString){
    String[] split = numberInString.split("");
    int sequenceLength = 0;
    if (split.length>0){
      if (split[0].equals(split[split.length-1])){
        sequenceLength++;
      }
    }
    for (int i = 1; i < split.length; i++) {
      if (!split[i-1].equals(split[i])){
        return sequenceLength;
      }
      sequenceLength++;
    }
    return sequenceLength;
  }

  @Override
  public String toString() {
    return "Plateau{" +
        "arrayOfIntegers=" + Arrays.toString(arrayOfIntegers) +
        ", sequence=" + sequence +
        ", sequenceLength=" + sequenceLength +
        ", sequenceIndex=" + sequenceIndex +
        '}';
  }

}
