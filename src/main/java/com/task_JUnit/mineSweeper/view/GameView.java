package com.task_JUnit.mineSweeper.view;

import com.task_JUnit.mineSweeper.controller.MainController;
import com.task_JUnit.mineSweeper.model.Game;

public class GameView {
  

  public void menu(){
    System.out.println("Enter game field size:");
  }


  public void printField(Game game){
    char[][] field = game.getGame();
    for (int i = 0; i<field.length; i++){
      for (int j = 0; j< field[i].length;j++){
        System.out.print(field[i][j]);
      }
      System.out.println();
    }
  }

  public void printCountedMinesField(Game game){
    char[][] gameField = game.getGame();
    for (int i = 1; i < gameField.length-1; i++) {
      for (int j = 1; j < gameField[i].length-1; j++){
        if (gameField[i][j] != '*'){
          int count = 0;
          if (gameField[i-1][j]=='*') count++;
          if (gameField[i+1][j]=='*') count++;
          if (gameField[i][j-1]=='*') count++;
          if (gameField[i][j+1]=='*') count++;
          if (gameField[i-1][j-1]=='*') count++;
          if (gameField[i+1][j+1]=='*') count++;
          if (gameField[i-1][j+1]=='*') count++;
          if (gameField[i+1][j-1]=='*') count++;
          System.out.print(count);
        }else System.out.print(gameField[i][j]);

//         if (gameField[i][j] == 1){
//           System.out.print("|*");
//         }else {
//           int neighbors = 0;
//           if (i==0){
//             if (i==gameField.length-1){
//               if (j==0){
//                 if (j==gameField[i].length-1){
//                   System.out.println(0);
//                 }else {
//                   System.out.println(gameField[i][j+1]);
//                 }
//               }else if (j==gameField[i].length-1){
//                 System.out.println(gameField[i][j-1]);
//               }else {
//                 System.out.println(gameField[i][j-1]+gameField[i][j+1]);
//               }
//             }else if(j==0){
//               if (j==gameField[i].length-1){
//                 System.out.println(gameField[i+1][j]);
//             }else {
//                 System.out.println(gameField[i+1][j+1]);
//               }
//             }
//           }else if (j==0){
//             if (j==gameField[i].length-1){
//               System.out.println();
//             }
//
//           }
         }
       System.out.println();
       }
    }
  }

