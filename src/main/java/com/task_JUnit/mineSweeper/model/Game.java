package com.task_JUnit.mineSweeper.model;

import java.util.Arrays;
import java.util.Random;

public class Game {

  private int columns;
  private int rows;
  private int b;
  private char[][] game;

  public int getColumns() {
    return columns;
  }

  public int getRows() {
    return rows;
  }

  public int getB() {
    return b;
  }

  public char[][] getGame() {
    return game;
  }

  public void setColumns(int columns) {
    this.columns = columns+2;
  }

  public void setRows(int rows) {
    this.rows = rows+2;
  }

  public void setB(int b) {
    this.b = b;
  }

  public char fillTheCell(){
    char c = '0';
    if (new Random().nextInt(this.b) != 0){
      c = '*';
    }
    return c;
  }

  public void generateGame(){
    char[][] game = new char[columns][rows];

    for (int k = 0; k < (rows); k++){
      game[0][k] = 0;
    }
    for (int l = 0; l < (rows); l++){
      game[columns-1][l] = 0;
    }
    for (int m = 0; m < (columns); m++){
      game[m][0] = 0;
    }
    for (int n = 0; n < (columns); n++){
      game[n][rows-1] = 0;
    }
    for (int i = 1; i < columns-1; i++) {
      for (int j = 1; j < rows-1; j++){
        game[i][j] = fillTheCell();
      }
    }
    this.game = game;
  }

  @Override
  public String toString() {
    return "Game{" +
        "columns=" + columns +
        ", rows=" + rows +
        ", b=" + b +
        ", game=" + Arrays.toString(game) +
        '}';
  }
}
