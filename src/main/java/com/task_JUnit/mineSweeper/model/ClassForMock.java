package com.task_JUnit.mineSweeper.model;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

public class ClassForMock {
  SomeService someService;

  public int doubleSum(int a, int b) {
    int sum = someService.sum(a, b);
    int doubleSum = sum * 2;
    return doubleSum;
  }
}
