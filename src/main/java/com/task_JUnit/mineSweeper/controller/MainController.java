package com.task_JUnit.mineSweeper.controller;

import com.task_JUnit.mineSweeper.model.Game;
import com.task_JUnit.mineSweeper.view.GameView;
import java.util.Arrays;
import java.util.Scanner;

public class MainController {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    GameView view = new GameView();
    Game game = new Game();

    game.setColumns(sc.nextInt());
    game.setRows(sc.nextInt());
    game.setB(sc.nextInt());
    game.generateGame();
    view.printField(game);
    view.printCountedMinesField(game);
  }
}
